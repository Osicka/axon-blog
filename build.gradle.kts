import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "nl.codecentric"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

plugins {
    id("org.springframework.boot") version "2.2.4.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"

    kotlin("jvm") version "1.3.61"
    kotlin("plugin.spring") version "1.3.61"
    kotlin("plugin.noarg") version "1.3.61"

}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // Mongo
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")

    // GraphQL
    implementation("com.expediagroup:graphql-kotlin-spring-server:2.0.0")
    implementation("com.expediagroup:graphql-kotlin-schema-generator:2.0.0")
    implementation("com.auth0:jwks-rsa:0.11.0")

    // Auth
    implementation("com.auth0:auth0:1.15.0")
    implementation("com.auth0:java-jwt:3.10.2")

    // Metrics
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("io.micrometer:micrometer-core")
    implementation("io.micrometer:micrometer-registry-prometheus")

    // Axon
    implementation("org.axonframework:axon-spring-boot-starter:4.2")
    implementation("org.axonframework.extensions.mongo:axon-mongo:4.2")
    implementation("org.axonframework:axon-micrometer:4.2")

    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
}

noArg {
    annotation("org.axonframework.spring.stereotype.Aggregate")
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"
    }
}

sourceSets {
    getByName("main").java.srcDirs("src/main/kotlin")
}

