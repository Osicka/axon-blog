package nl.codecentric.banq.domain.transaction

import java.math.BigDecimal

data class TransactionRequestedEvent(
    val transactionId: String,
    val sourceBankAccountId: String,
    val targetBankAccountId: String,
    val amount: BigDecimal
)

data class TransactionCompletedEvent(
    val transactionId: String
)

data class TransactionCancelledEvent(
    val transactionId: String,
    val reason: String?
)
