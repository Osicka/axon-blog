package nl.codecentric.banq.domain.transaction

import nl.codecentric.banq.domain.bankaccount.CreditCommand
import nl.codecentric.banq.domain.bankaccount.CreditedEvent
import nl.codecentric.banq.domain.bankaccount.DebitCommand
import nl.codecentric.banq.domain.bankaccount.DebitedEvent
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.modelling.saga.EndSaga
import org.axonframework.modelling.saga.SagaEventHandler
import org.axonframework.modelling.saga.SagaLifecycle
import org.axonframework.modelling.saga.SagaLifecycle.end
import org.axonframework.modelling.saga.StartSaga
import org.axonframework.spring.stereotype.Saga
import org.springframework.beans.factory.annotation.Autowired

@Saga
class TransactionSaga {
    @Autowired
    @Transient
    private lateinit var commandGateway: CommandGateway
    lateinit var transactionId: String
    lateinit var targetBankAccountId: String

    @StartSaga
    @SagaEventHandler(associationProperty = "transactionId")
    fun on(event: TransactionRequestedEvent) {
        SagaLifecycle.associateWith("transactionId", event.transactionId)
        this.transactionId = event.transactionId
        this.targetBankAccountId = event.targetBankAccountId

        try {
            commandGateway.sendAndWait<CreditCommand>(
                    CreditCommand(event.sourceBankAccountId, transactionId, event.amount)
            )
        } catch (e: Exception) {
            commandGateway.sendAndWait<CancelTransactionCommand>(
                    CancelTransactionCommand(transactionId, e.message)
            )
        }
    }

    @SagaEventHandler(associationProperty = "transactionId")
    fun on(event: CreditedEvent) {
        commandGateway.sendAndWait<DebitCommand>(
                DebitCommand(targetBankAccountId, event.transactionId, event.amount)
        )
    }

    @EndSaga
    @SagaEventHandler(associationProperty = "transactionId")
    fun on(event: DebitedEvent) {
        commandGateway.sendAndWait<CompleteTransactionCommand>(CompleteTransactionCommand(event.transactionId))
    }

    @SagaEventHandler(associationProperty = "transactionId")
    fun on(event: TransactionCancelledEvent) {
        end()
    }
}
