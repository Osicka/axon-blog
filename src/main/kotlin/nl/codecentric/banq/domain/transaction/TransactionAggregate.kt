package nl.codecentric.banq.domain.transaction

import java.util.UUID
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.modelling.command.AggregateLifecycle.markDeleted
import org.axonframework.spring.stereotype.Aggregate

@Aggregate
class TransactionAggregate {
    @AggregateIdentifier
    lateinit var transactionId: String

    // CommandHandlers
    @CommandHandler
    constructor(command: TransferCommand) {
        AggregateLifecycle.apply(
                TransactionRequestedEvent(
                        command.transactionId,
                        command.sourceBankAccountId,
                        command.targetBankAccountId,
                        command.amount
                )
        )
    }

    @CommandHandler
    fun on(command: CompleteTransactionCommand) {
        AggregateLifecycle.apply(
                TransactionCompletedEvent(command.transactionId)
        )
    }

    @CommandHandler
    fun on(command: CancelTransactionCommand) {
        AggregateLifecycle.apply(
                TransactionCancelledEvent(command.transactionId, command.reason)
        )
    }

    // EventHandlers
    @EventSourcingHandler
    fun on(event: TransactionRequestedEvent) {
        this.transactionId = event.transactionId
    }

    @EventSourcingHandler
    fun on(event: TransactionCompletedEvent) {
        markDeleted()
    }
}
