package nl.codecentric.banq.domain.bankaccount

data class FetchAllBankAccountsForCustomerId(val customerId: String)

data class FetchBankAccountById(val bankAccountId: String, val customerId: String)

data class IsBankAccountFromCustomerQuery(val bankAccountId: String, val customerId: String)
