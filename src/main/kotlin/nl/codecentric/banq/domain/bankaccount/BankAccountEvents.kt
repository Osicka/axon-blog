package nl.codecentric.banq.domain.bankaccount

import java.math.BigDecimal
import java.util.UUID

interface BankAccountEvent {
    val bankAccountId: String
}

data class BankAccountCreatedEvent(override val bankAccountId: String, val owner: String) : BankAccountEvent
data class DepositedEvent(override val bankAccountId: String, val amount: BigDecimal) : BankAccountEvent
data class WithdrawalEvent(override val bankAccountId: String, val amount: BigDecimal) : BankAccountEvent
data class DebitedEvent(override val bankAccountId: String, val transactionId: String, val amount: BigDecimal) : BankAccountEvent
data class CreditedEvent(override val bankAccountId: String, val transactionId: String, val amount: BigDecimal) : BankAccountEvent
