package nl.codecentric.banq.domain

import java.util.UUID
import nl.codecentric.banq.domain.bankaccount.CreateBankAccountCommand
import nl.codecentric.banq.domain.customer.CustomerRegisteredEvent
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.EventHandler
import org.springframework.stereotype.Service

@Service
@ProcessingGroup("BankAccountManager")
class BankAccountManager(private val commandGateway: CommandGateway) {
    @EventHandler
    fun on(event: CustomerRegisteredEvent) {
        commandGateway.send<CreateBankAccountCommand>(CreateBankAccountCommand(UUID.randomUUID().toString(), event.id))
    }
}
