package nl.codecentric.banq.domain.customer

data class CustomerRegisteredEvent(
    val id: String,
    val username: String,
    val password: String
)
