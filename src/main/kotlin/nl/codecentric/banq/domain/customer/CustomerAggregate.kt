package nl.codecentric.banq.domain.customer

import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventhandling.EventHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate

@Aggregate
class CustomerAggregate {
    @AggregateIdentifier
    lateinit var id: String

    @CommandHandler
    constructor(command: RegisterCustomerCommand) {
        AggregateLifecycle.apply(
                CustomerRegisteredEvent(
                        command.id,
                        command.username,
                        command.password
                )
        )
    }

    @EventHandler
    fun on(event: CustomerRegisteredEvent) {
        this.id = event.id
    }
}
