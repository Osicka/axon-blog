package nl.codecentric.banq.domain.customer

data class FetchAllCustomersQuery(private val id: String = "")
