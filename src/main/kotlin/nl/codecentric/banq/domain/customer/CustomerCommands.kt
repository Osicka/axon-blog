package nl.codecentric.banq.domain.customer

import org.axonframework.modelling.command.TargetAggregateIdentifier

data class RegisterCustomerCommand(
    @TargetAggregateIdentifier
    val id: String,
    val username: String,
    val password: String
)
