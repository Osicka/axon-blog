package nl.codecentric.banq.http.graphql

import com.expediagroup.graphql.execution.GraphQLContext
import com.expediagroup.graphql.spring.execution.GraphQLContextFactory
import org.axonframework.queryhandling.QueryGateway
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.stereotype.Component

@Component
class GraphQLContextFactory(val queryGateway: QueryGateway) : GraphQLContextFactory<Context> {
    override suspend fun generateContext(request: ServerHttpRequest, response: ServerHttpResponse): Context {
        return Context(queryGateway, AuthService(request, response))
    }
}

class Context(val queryGateway: QueryGateway, val auth: AuthService) : GraphQLContext
