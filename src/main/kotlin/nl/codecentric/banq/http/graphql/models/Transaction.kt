package nl.codecentric.banq.http.graphql.models

import nl.codecentric.banq.projections.transaction.Status
import nl.codecentric.banq.projections.transaction.TransactionType
import java.math.BigDecimal

class Transaction(val transactionId: String,
                  val sourceBankAccountId: String?,
                  val targetBankAccountId: String?,
                  val amount: BigDecimal,
                  var status: Status,
                  var transactionType: TransactionType) {
}