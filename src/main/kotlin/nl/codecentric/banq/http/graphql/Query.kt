package nl.codecentric.banq.http.graphql

import com.expediagroup.graphql.spring.operations.Query
import nl.codecentric.banq.domain.bankaccount.FetchAllBankAccountsForCustomerId
import nl.codecentric.banq.domain.bankaccount.FetchBankAccountById
import nl.codecentric.banq.domain.customer.FetchAllCustomersQuery
import nl.codecentric.banq.http.graphql.models.BankAccount
import nl.codecentric.banq.http.graphql.models.Customer
import nl.codecentric.banq.projections.bankaccount.BankAccountDTO
import nl.codecentric.banq.projections.customer.CustomerDTO
import org.axonframework.messaging.responsetypes.InstanceResponseType
import org.axonframework.messaging.responsetypes.MultipleInstancesResponseType
import org.axonframework.queryhandling.QueryGateway
import org.springframework.stereotype.Component
import java.util.concurrent.CompletableFuture

@Component
class Query(private val queryGateway: QueryGateway) : Query {
    fun customers(context: Context): CompletableFuture<List<Customer>> {
        context.auth.isAuthenticatedOrThrow()
        return queryGateway
                .query(FetchAllCustomersQuery(), MultipleInstancesResponseType(CustomerDTO::class.java))
                .thenApply { customers -> customers.map { Customer(it.id, it.username, it.password) } }
    }

    fun bankAccounts(context: Context): CompletableFuture<List<BankAccount>> {
        val customerId = context.auth.getCustomerIdOrThrow()
        return queryGateway
                .query(FetchAllBankAccountsForCustomerId(customerId), MultipleInstancesResponseType(BankAccountDTO::class.java))
                .thenApply { bankAccounts -> bankAccounts.map { BankAccount(it.bankAccountId, it.customerId, it.balance) } }
    }

    fun bankAccount(bankAccountId: String, context: Context): CompletableFuture<BankAccount> {
        val customerId = context.auth.getCustomerIdOrThrow()
        return queryGateway
                .query(FetchBankAccountById(bankAccountId, customerId), InstanceResponseType(BankAccountDTO::class.java))
                .thenApply { BankAccount(it.bankAccountId, it.customerId, it.balance) }
    }
}
