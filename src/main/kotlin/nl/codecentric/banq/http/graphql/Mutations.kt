package nl.codecentric.banq.http.graphql

import com.auth0.json.auth.TokenHolder
import com.expediagroup.graphql.spring.operations.Mutation
import com.fasterxml.jackson.annotation.JsonProperty
import graphql.ErrorType
import graphql.language.SourceLocation
import nl.codecentric.banq.domain.bankaccount.DepositCommand
import nl.codecentric.banq.domain.bankaccount.IsBankAccountFromCustomerQuery
import nl.codecentric.banq.domain.bankaccount.WithdrawCommand
import nl.codecentric.banq.domain.bankaccount.WithdrawalEvent
import nl.codecentric.banq.domain.customer.FetchAllCustomersQuery
import nl.codecentric.banq.domain.customer.RegisterCustomerCommand
import nl.codecentric.banq.domain.transaction.TransferCommand
import nl.codecentric.banq.http.graphql.exceptions.BaseException
import nl.codecentric.banq.projections.customer.CustomerDTO
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.messaging.responsetypes.MultipleInstancesResponseType
import org.axonframework.queryhandling.DefaultQueryGateway
import org.axonframework.queryhandling.QueryGateway
import org.springframework.stereotype.Component
import java.math.BigDecimal

@Component
class Mutations(private val commandGateway: CommandGateway, private val queryGateway: QueryGateway) : Mutation {
    fun createCustomer(command: RegisterCustomerRequest): Boolean {
        this.commandGateway.sendAndWait<RegisterCustomerCommand>(
                RegisterCustomerCommand(command.id, command.username, command.password)
        )
        return true
    }

    fun makeDeposit(input: MakeDepositInput): Boolean {
        this.commandGateway.sendAndWait<DepositCommand>(
                DepositCommand(input.bankAccountId, input.amount)
        )
        return true
    }

    fun makeWithdrawal(input: MakeWithdrawalInput): Boolean {
        this.commandGateway.sendAndWait<WithdrawalEvent>(
                WithdrawCommand(input.bankAccountId, input.amount)
        )
        return true
    }

    fun login(input: LoginInput, context: Context): LoginResponse {
        val result = context.auth.login(input.username, input.password)
        return LoginResponse(result?.idToken)
    }

    fun register(input: RegisterInput, context: Context): Boolean {
        context.auth.register(input.customerId, input.username, input.password)
        commandGateway.send<RegisterCustomerCommand>(RegisterCustomerCommand(input.customerId, input.username, input.password))
        return true
    }

    fun transfer(input: TransferInput, context: Context): Boolean {
        val customerId = context.auth.getCustomerIdOrThrow()
        queryGateway
                .query(IsBankAccountFromCustomerQuery(input.bankAccountId, customerId), Boolean::class.java)
                .thenApply { if(it) {
                    commandGateway.send<TransferCommand>(TransferCommand(input.transactionId, input.bankAccountId, input.targetBankAccountId, input.amount))
                } else {
                    throw BankAccountCustomerMismatch()
                }}
        return true
    }
}

data class LoginInput(val username: String, val password: String)

data class LoginResponse(
        val idToken: String?
)

data class RegisterInput(val customerId: String, val username: String, val password: String)

data class RegisterCustomerRequest(
        val id: String,
        val username: String,
        val password: String
)

data class MakeDepositInput(val bankAccountId: String, val amount: BigDecimal)

data class MakeWithdrawalInput(val bankAccountId: String, val amount: BigDecimal)

data class TransferInput(
        val bankAccountId: String,
        val targetBankAccountId: String,
        val transactionId: String,
        val amount: BigDecimal
)

open class BankAccountCustomerMismatch : BaseException() {
    override fun errorMessage(): String = "The bank account you wanted to make an transaction on is not yours."
    override fun getErrorType(): ErrorType = ErrorType.ExecutionAborted
    override fun getLocations(): MutableList<SourceLocation>? = null
}