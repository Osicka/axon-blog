package nl.codecentric.banq.http.graphql

import com.auth0.client.auth.AuthAPI
import com.auth0.exception.Auth0Exception
import com.auth0.json.auth.TokenHolder
import com.auth0.jwk.Jwk
import com.auth0.jwk.JwkProvider
import com.auth0.jwk.UrlJwkProvider
import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import graphql.ErrorType
import graphql.language.SourceLocation
import nl.codecentric.banq.http.graphql.exceptions.BaseException
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import java.security.interfaces.RSAPublicKey

class AuthService(private val request: ServerHttpRequest, private val response: ServerHttpResponse) {
    private val auth: AuthAPI = AuthAPI("https://dev-4pew2kna.eu.auth0.com",
            "hOCkybdmjVQF2szS2XmFtQuwc5V1mpPb",
            "8qLZZLIzyaCzQcROYyNJoJ6xeyzpQvHVnULxurx446QlMDdX6zlIWiyn410DAgWX"
    )
    private val provider: JwkProvider = UrlJwkProvider("https://dev-4pew2kna.eu.auth0.com")
    private var isAuthenticated = false
    private var customerId: String? = null

    init {
        val token = request.headers["Authorization"]
        if (token != null && token.size == 1 && token.first().length > 7) {
            val decoded = JWT.decode(token.first().substring(7))
            val jwk: Jwk = provider.get(decoded.keyId)
            val algorithm = Algorithm.RSA256(jwk.publicKey as RSAPublicKey, null)
            val verifier = JWT.require(algorithm)
            try {
                val verify = verifier.build().verify(decoded)
                this.customerId = verify.getClaim("https://patrickdronk.dev/customerId").asString()
                this.isAuthenticated = true
            } catch (e: JWTVerificationException) {
                this.isAuthenticated = false
            }
        }
    }

    fun register(customerId: String, username: String, password: String): Boolean {
        try {
            this.auth.signUp(username, password, "Username-Password-Authentication")
                    .setCustomFields(mapOf("customerId" to customerId))
                    .execute()
        } catch (e: Auth0Exception) {
            print(e)
            return false
        }
        return true
    }

    fun login(username: String, password: String): TokenHolder? {
        try {
            return auth.login(username, password).execute()
        } catch (e: Auth0Exception) {
            throw AuthorizationException("login.failed", "Login failed")
        }
    }

    fun isAuthenticatedOrThrow() {
        if (!this.isAuthenticated) {
            throw AuthorizationException("authorization.failed", "You are not logged in")
        }
    }

    fun getCustomerIdOrThrow(): String {
        if (this.customerId.isNullOrBlank()) {
            throw AuthorizationException("authorization.failed", "You are not logged in")
        }
        return this.customerId!!
    }
}

open class AuthorizationException(private val errorCode: String, private val errorMessage: String) : BaseException() {
    override fun errorMessage(): String = "Unauthorized"
    override fun getErrorType(): ErrorType = ErrorType.ExecutionAborted
    override fun getLocations(): MutableList<SourceLocation>? = null
    override fun getExtensions(): Map<String, String> {
        return mapOf(
                "errorCode" to errorCode,
                "errorMessage" to errorMessage
        )
    }
}