package nl.codecentric.banq.http.rest.customer

data class RegisterCustomerRequest(
    val id: String,
    val username: String,
    val password: String
)
