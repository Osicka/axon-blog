package nl.codecentric.banq.http.rest.replaying

import nl.codecentric.banq.projections.Replayer
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ReplayController(private val replayer: Replayer) {
    @GetMapping("/replay")
    fun getAllUsers(): Boolean {
        replayer.replay("TransactionProjection")
        return true
    }
}
