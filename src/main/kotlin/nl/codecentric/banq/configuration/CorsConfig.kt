package nl.codecentric.banq.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsWebFilter
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource

@Configuration
class WebConfig {
    @Bean
    fun corsFilter(): CorsWebFilter = CorsWebFilter(
            UrlBasedCorsConfigurationSource().apply {
                registerCorsConfiguration(
                        "/**",
                        CorsConfiguration().apply {
                            allowCredentials = true
                            allowedOrigins = listOf("*")
                            allowedHeaders = listOf("*")
                            allowedMethods = listOf("*")
                        }
                )
            }
    )
}