package nl.codecentric.banq

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BanqApplication

fun main(args: Array<String>) {
    runApplication<BanqApplication>(*args)
}
