package nl.codecentric.banq.projections.customer

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class CustomerDTO(
    @Id
    val id: String,
    val username: String,
    val password: String
)
