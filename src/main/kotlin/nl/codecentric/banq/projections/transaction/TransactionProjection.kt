package nl.codecentric.banq.projections.transaction

import java.util.UUID
import nl.codecentric.banq.domain.bankaccount.DepositedEvent
import nl.codecentric.banq.domain.bankaccount.WithdrawalEvent
import nl.codecentric.banq.domain.transaction.FetchAllTransactionsForBankAccountId
import nl.codecentric.banq.domain.transaction.TransactionCompletedEvent
import nl.codecentric.banq.domain.transaction.TransactionRequestedEvent
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component
@ProcessingGroup("TransactionProjection")
class TransactionProjection(private val repository: TransactionRepository) {

    @EventHandler
    fun on(event: DepositedEvent) {
        this.repository.save(TransactionDTO(
                UUID.randomUUID().toString(),
                null,
                event.bankAccountId,
                event.amount,
                Status.FINISHED,
                TransactionType.DEPOSIT
        ))
    }

    @EventHandler
    fun on(event: WithdrawalEvent) {
        this.repository.save(TransactionDTO(
                UUID.randomUUID().toString(),
                null,
                event.bankAccountId,
                -event.amount,
                Status.FINISHED,
                TransactionType.WITHDRAWAL
        ))
    }

    @EventHandler
    fun on(event: TransactionRequestedEvent) {
        val transaction = TransactionDTO(
                event.transactionId,
                event.sourceBankAccountId,
                event.targetBankAccountId,
                event.amount,
                Status.IN_PROGRESS,
                TransactionType.BANK_TRANSACTION
        )
        repository.save(transaction)
    }

    @EventHandler
    fun on(event: TransactionCompletedEvent) {
        val transaction = repository.findByIdOrNull(event.transactionId)
        if (transaction != null) {
            transaction.status = Status.FINISHED
            repository.save(transaction)
        }
    }

    @QueryHandler
    fun on(query: FetchAllTransactionsForBankAccountId): List<TransactionDTO> {
        return repository.findAllBySourceBankAccountIdOrTargetBankAccountId(query.bankAccountId, query.bankAccountId)
    }
}
