package nl.codecentric.banq.projections.transaction

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal

@Document
data class TransactionDTO(
    @Id
    val transactionId: String,
    val sourceBankAccountId: String?,
    val targetBankAccountId: String?,
    val amount: BigDecimal,
    var status: Status,
    var transactionType: TransactionType
)

enum class TransactionType {
    DEPOSIT,
    WITHDRAWAL,
    BANK_TRANSACTION
}
enum class Status {
    IN_PROGRESS,
    FINISHED,
    CANCELLED
}
