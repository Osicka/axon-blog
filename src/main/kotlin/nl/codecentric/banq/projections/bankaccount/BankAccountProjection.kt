package nl.codecentric.banq.projections.bankaccount

import com.sun.org.apache.xpath.internal.operations.Bool
import java.math.BigDecimal
import java.util.Optional
import nl.codecentric.banq.domain.bankaccount.BankAccountCreatedEvent
import nl.codecentric.banq.domain.bankaccount.CreditedEvent
import nl.codecentric.banq.domain.bankaccount.DebitedEvent
import nl.codecentric.banq.domain.bankaccount.DepositedEvent
import nl.codecentric.banq.domain.bankaccount.FetchAllBankAccountsForCustomerId
import nl.codecentric.banq.domain.bankaccount.FetchBankAccountById
import nl.codecentric.banq.domain.bankaccount.IsBankAccountFromCustomerQuery
import nl.codecentric.banq.domain.bankaccount.WithdrawalEvent
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.EventHandler
import org.axonframework.eventhandling.ResetHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component
import java.lang.Exception

@Component
@ProcessingGroup("BankAccountProjection")
class BankAccountProjection(private val bankAccountRepository: BankAccountRepository) {
    @EventHandler
    fun on(event: BankAccountCreatedEvent) {
        bankAccountRepository.save(
                BankAccountDTO(event.bankAccountId, event.owner, BigDecimal.ZERO)
        )
    }

    @EventHandler
    fun on(event: DepositedEvent) {
        val bankAccount = bankAccountRepository.findByIdOrNull(event.bankAccountId)
        if (bankAccount != null) {
            bankAccount.balance = bankAccount.balance.add(event.amount)
            bankAccountRepository.save(bankAccount)
        }
    }

    @EventHandler
    fun on(event: WithdrawalEvent) {
        val bankAccount = bankAccountRepository.findByIdOrNull(event.bankAccountId)
        if (bankAccount != null) {
            bankAccount.balance = bankAccount.balance.minus(event.amount)
            bankAccountRepository.save(bankAccount)
        }
    }

    @EventHandler
    fun on(event: DebitedEvent) {
        val bankAccount = bankAccountRepository.findByIdOrNull(event.bankAccountId)
        if (bankAccount != null) {
            bankAccount.balance = bankAccount.balance.add(event.amount)
            bankAccountRepository.save(bankAccount)
        }
    }

    @EventHandler
    fun on(event: CreditedEvent) {
        val bankAccount = bankAccountRepository.findByIdOrNull(event.bankAccountId)
        if (bankAccount != null) {
            bankAccount.balance = bankAccount.balance.minus(event.amount)
            bankAccountRepository.save(bankAccount)
        }
    }

    @QueryHandler
    fun on(query: FetchAllBankAccountsForCustomerId): List<BankAccountDTO> {
        return bankAccountRepository.findBankAccountsByCustomerId(query.customerId)
    }

    @QueryHandler
    fun on(query: FetchBankAccountById): Optional<BankAccountDTO> {
        return bankAccountRepository.findById(query.bankAccountId)
    }

    @QueryHandler
    fun on(query: IsBankAccountFromCustomerQuery): Boolean {
        return try {
            val found = bankAccountRepository.findBankAccountDTOByBankAccountIdAndCustomerId(query.bankAccountId, query.customerId)
            found == null
        } catch (e: Exception) {
            print(e)
            false
        }
    }

    @ResetHandler
    fun onReset() {
        bankAccountRepository.deleteAll()
    }
}
