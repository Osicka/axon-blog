package nl.codecentric.banq.projections.bankaccount

import java.math.BigDecimal
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoId

@Document
data class BankAccountDTO(@MongoId val bankAccountId: String, val customerId: String, var balance: BigDecimal)
