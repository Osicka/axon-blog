package nl.codecentric.banq.projections.bankaccount

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface BankAccountRepository : MongoRepository<BankAccountDTO, String> {
    fun findBankAccountsByCustomerId(customerId: String): List<BankAccountDTO>

    fun findBankAccountDTOByBankAccountIdAndCustomerId(bankAccountId: String, customerId: String): BankAccountDTO?
}
