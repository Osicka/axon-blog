# Axon Banq
This is the backend for the Banq. 
It's an Axon application with GraphQL as access layer to the Command and Query bus

 
## Running the Banq
First run the application by running `docker-compose up` from the root directory. 
It will create two services. First, it will create Axon server. Second, MongoDB for all our projections.
You can access Axon-server at [localhost:8024](http://localhost:8024)
You can access the MongoDB at localhost:27017 with username `mongo` and password `mongo`.

## Domain
The Banq application is now divided in three different domains

⋅⋅* customer
⋅⋅* bank account
⋅⋅* transaction

The basic flow is a customer wants to register and currently when a registration was successful, 
The customer gets a bank account provided by the bank account manager.
Then, a deposit, withdrawal, or transaction from or to this bank account can be made. 
The customer starts with a default balance of 0. The balance can never go negative. 
Eventually a customer can close its account, but only if the balance is 0.




